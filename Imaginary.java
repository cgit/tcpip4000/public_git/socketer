class Imaginary {
	double real;
	double imaginary;

	public Imaginary() {
		this.real = 0;
		this.imaginary = 0;
	}

	public Imaginary(double r, double i) {
		this.real = r;
		this.imaginary = i;
	}

	public double getReal() {
		return this.real;
	}

	public double getImaginary() {
		return this.imaginary;
	}

	public void setReal(double r) {
		this.real = r;
	}

	public void setImaginary(double i) {
		this.imaginary = i;
	}
}

